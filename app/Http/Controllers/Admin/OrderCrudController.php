<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CreateOrderRequest as StoreRequest;
use App\Http\Requests\UpdateOrderRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;


use  App\Models\Order;
use Illuminate\Support\Facades\DB;


/**
 * Class OrderCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class OrderCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Order');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/order');
        $this->crud->setEntityNameStrings('order', 'orders');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        // add asterisk for fields that are required in OrderRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        // ========================== new ================================
        $this->crud->removeField('purchase_date', 'service_name', 'status', 'service_response');
        $this->crud->addField([
            'name' => 'service_name',
            'type' => 'enum',
        ])->addField([
            'name' => 'status',
            'type' => 'enum',
        ])->addField([
            'name' => 'purchase_date',
            'type' => 'date_picker',
        ]);

        $this->crud->removeField(['service_code', 'promo_code', 'service_name'], 'update');

    }

    public function store(StoreRequest $request)
    {
        $redirect_location = parent::storeCrud($request);
        // code after save
        // 
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
