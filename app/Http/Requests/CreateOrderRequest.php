<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'name' => 'required|string|min:5|max:255',
             'reason' => 'required|string|min:5|max:255',
             'service_code' => 'required|min:2|max:255',
             'purchase_date' => 'required',
             'fone' => 'regex:/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/|required',
             'service_code' => 'required|string|min:1|max:128',
             'promo_code' => 'required|string|min:1|max:128',
             'service_name' => ['required', Rule::in(['Лимон', 'Апельсин', 'Мандарин'])],
             'status' => ['required', Rule::in(['На согласовании', 'Пришел ответ', 'Замена утверждена', 'В замене отказано'])],
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
