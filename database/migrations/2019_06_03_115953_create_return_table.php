<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->char('name', 255);
            $table->char('reason', 255);
            $table->date('purchase_date');
            $table->char('service_code', 128);
            $table->char('promo_code', 128);
            $table->char('fone', 100);
            $table->enum('service_name', ['Лимон', 'Апельсин', 'Мандарин']);
            $table->enum('status', ['На согласовании', 'Пришел ответ', 'Замена утверждена', 'В замене отказано'])->default('На согласовании');
            $table->char('service_response', 128)->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
